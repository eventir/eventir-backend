# Eventir Backend
Backend for the eventir application. 
An API for event management and attendance tracking.

For an overview of the application and screenshots see the [Eventir frontend repository](https://gitlab.com/eventir/eventir-frontend)

## Setup
In order to run a local version of the backend, you'll need to setup the following:

 1. A local MySQL database running on localhost. You will need to create credentials for a user named `eventir` that has `SELECT`, `INSERT`, `UPDATE`, and `DELETE` privileges on the `eventir` database. You will need to store the user's password in a system environment variable titled `EVENTIR_DATABASE_USER_PASSWORD`.
 2. A system environment variable titled `EVENTIR_OPENID_CLIENT_SECRET` that contains the [Eventir OAuth 2.0 client secret](https://console.developers.google.com/apis/credentials/oauthclient/648937767042-5pk9ka7a5nbou6mk48d6p7237mb5rr8i.apps.googleusercontent.com?project=eventir&authuser=1&folder&organizationId). This is required for our Google OIDC authentication to work.
 3. The password to the eventirsystem@gmail.com account, stored in an environment variable named `EVENTIR_SMTP_PASSWORD`

## Run

Linux/OS X:

```
sh build.sh
```

Windows:

```
build.bat
```

## Run in Docker

```
cd src/Org.OpenAPITools
docker build -t org.openapitools .
docker run -p 5000:5000 org.openapitools
```

## Notes
To temporarily disable authentication for a controller, comment out the `[authorize]` tag at the top of its source code.