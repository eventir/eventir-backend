using System;
using System.ComponentModel.DataAnnotations;

namespace Org.OpenAPITools
{
    public enum LogActions
    {
        [Display(Name = "created")] CREATED,
        [Display(Name = "updated")] UPDATED,
        [Display(Name = "deleted")] DELETED,
        [Display(Name = "attempted to create")] ATTEMPT_CREATE,
        [Display(Name = "attempted to update")] ATTEMPT_UPDATE,
        [Display(Name = "attempted to delete")] ATTEMPT_DELETE
    }

    static class LogActionStrings
    {
        public static string ToFriendlyString(this LogActions actions)
        {
            switch (actions)
            {
                case LogActions.CREATED:
                    return "created";
                case LogActions.UPDATED:
                    return "updated";
                case LogActions.DELETED:
                    return "deleted";
                case LogActions.ATTEMPT_CREATE:
                    return "attempted to create";
                case LogActions.ATTEMPT_UPDATE:
                    return "attempted to update";
                case LogActions.ATTEMPT_DELETE:
                    return "attempted to delete";
            }

            return "no op";
        }
    }
}