using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{
    [Route("api/rsvps")]
    [Authorize]
    public class RsvpController : ControllerBase
    {
        private DatabaseSettings db;
        private IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public RsvpController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }

        public class RsvpPostModel
        {
            public string userId;
            public int sessionId;
        }

        /// <summary>
        /// Create a rsvp
        /// </summary>
        /// <remarks>Creates a new instance of a &#x60;rsvp&#x60;.</remarks>
        /// <response code="201">Successful response.</response>
        [HttpPost]
        [ValidateModelState]
        [SwaggerOperation("CreateRsvp")]
        public async Task<IActionResult> CreateRsvp([FromBody] RsvpPostModel model)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "rsvp", model);
            if (!await auth.IsSessionAdmin(UserId, model.sessionId) &&
                UserId != model.userId)
                return Unauthorized();
            string sql =
                @"INSERT INTO Rsvps (userId, sessionId) VALUES (@userId, @sessionId);
                  SELECT * FROM Rsvps WHERE rsvpId = LAST_INSERT_ID();";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new
                {
                    model.userId,
                    model.sessionId
                });
                if (result.Any())
                {
                    var newRsvp = result.FirstOrDefault();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "rsvp", newRsvp);
                    return CreatedAtRoute("GetRsvp", new {rsvpId = newRsvp.rsvpId}, newRsvp);
                }
                else
                    return UnprocessableEntity();
            }
        }

        /// <summary>
        /// Delete a rsvp
        /// </summary>
        /// <remarks>Deletes an existing &#x60;rsvp&#x60;.</remarks>
        /// <param name="userId">The user for which to remove a RSVP</param>
        /// <param name="sessionId">The session for which to remove a RSVP</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("")]
        [ValidateModelState]
        [SwaggerOperation("DeleteRsvp")]
        public async Task<IActionResult> DeleteRsvp([FromBody] RsvpPostModel model)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "rsvp", model);
            var sql = "DELETE FROM Rsvps WHERE userId = @userId AND sessionId = @sessionId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                if (!await auth.IsSessionAdmin(UserId, model.sessionId) &&
                    UserId != model.userId)
                    return Unauthorized();

                var deleteResult = await conn.ExecuteAsync(sql, new {model.userId, model.sessionId});
                if (deleteResult > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "rsvp", model);
                    return StatusCode(204);
                }
                else
                    return StatusCode(500);
            }
        }

        /// <summary>
        /// Get a rsvp
        /// </summary>
        /// <remarks>Gets the details of a single instance of a &#x60;rsvp&#x60;.</remarks>
        /// <param name="rsvpId">A unique identifier for a &#x60;rsvp&#x60;.</param>
        /// <response code="200">Successful response - returns a single &#x60;rsvp&#x60;.</response>
        [HttpGet("{rsvpId}", Name = "GetRsvp")]
        [ValidateModelState]
        [SwaggerOperation("GetRsvp")]
        [SwaggerResponse(statusCode: 200, type: typeof(Rsvp),
            description: "Successful response - returns a single &#x60;rsvp&#x60;.")]
        public async Task<IActionResult> GetRsvp([FromRoute][Required] int rsvpId)
        {
            string sql =
                @"SELECT * FROM Rsvps WHERE rsvpId = @rsvpId;
                  SELECT * FROM Users JOIN Rsvps using (userId) where Rsvps.rsvpId = @rsvpId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new {rsvpId});
                var rsvp = result.ReadFirstOrDefault<Rsvp>();
                if (rsvp == null)
                    return NotFound();
                rsvp.User = result.ReadSingle<User>();

                return Ok(rsvp);
            }
        }
    }
}