using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{

    [Route("api/events")]
    [Authorize]
    public class EventController : ControllerBase
    {
        private DatabaseSettings db;
        private IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public EventController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }

        /// <summary>
        /// Create a event
        /// </summary>
        /// <remarks>Creates a new instance of a &#x60;event&#x60;.</remarks>
        /// <param name="ev">A new &#x60;event&#x60; to be created.</param>
        /// <response code="201">Successful response.</response>
        [HttpPost]
        [ValidateModelState]
        [SwaggerOperation("CreateEvent")]
        [SwaggerResponse(statusCode: 201, type: typeof(Event), description: "Successful response.")]
        public async Task<IActionResult> CreateEvent([FromBody]Event ev)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "event", ev);
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql =
                @"INSERT INTO Events (name, description, image, start, end, checkinWindow)
                    VALUES (@name, @description, @image, @start, @end, @checkinWindow);
                  SELECT * FROM Events WHERE eventId = LAST_INSERT_ID();";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, ev);
                if (result.Any())
                {
                    var newEvent = result.First();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "event", newEvent);
                    return CreatedAtRoute("GetEvent", new { eventId = newEvent.eventId }, newEvent);
                }
                else
                    return UnprocessableEntity();
            }
        }

        /// <summary>
        /// Delete a event
        /// </summary>
        /// <remarks>Deletes an existing &#x60;event&#x60;.</remarks>
        /// <param name="eventId">A unique identifier for a &#x60;event&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("{eventId}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEvent")]
        public async Task<IActionResult> DeleteEvent([FromRoute][Required] int eventId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "event", new {eventId});
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql = "DELETE FROM Events WHERE eventId = @eventId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new { eventId });
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "event", new {eventId});
                    return StatusCode(204);
                }
                else
                    return StatusCode(500);
            }
        }

        /// <summary>
        /// Get a event
        /// </summary>
        /// <remarks>Gets the details of a single instance of a &#x60;event&#x60;.</remarks>
        /// <param name="eventId">A unique identifier for a &#x60;event&#x60;.</param>
        /// <response code="200">Successful response - returns a single &#x60;event&#x60;.</response>
        [HttpGet("{eventId}", Name = "GetEvent")]
        [ValidateModelState]
        [SwaggerOperation("GetEvent")]
        [SwaggerResponse(statusCode: 200, type: typeof(Event), description: "Successful response - returns a single &#x60;event&#x60;.")]
        public async Task<IActionResult> GetEvent([FromRoute] [Required] int eventId)
        {
            string sql =
                 @"SELECT * FROM Events WHERE eventId = @eventId;
                   SELECT * FROM Sessions WHERE eventId = @eventId ORDER BY sessionId ASC;
                   SELECT * FROM EventAdmins INNER JOIN Users ON EventAdmins.userId = Users.userId WHERE eventId = @eventId;
                   SELECT firstName, lastName, Users.userId, email, profileIcon, SessionAdmins.sessionId FROM SessionAdmins
                     INNER JOIN Users ON SessionAdmins.userId = Users.userId INNER JOIN Sessions ON SessionAdmins.sessionId = Sessions.sessionId
                     WHERE eventId = @eventId ORDER BY sessionId ASC;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new { eventId });
                var ev = result.ReadSingle<Event>();
                ev.Sessions = result.Read<Session>().Distinct().ToList();
                foreach (var session in ev.Sessions)
                {
                    session.Rsvps = new List<Rsvp>();
                    session.SessionAdmins = new List<User>();
                }
                ev.EventAdmins = result.Read<User>().Distinct().ToList();
                var sessionAdmins = result.Read().Distinct().ToList();
                var index = 0;
                foreach (var admin in sessionAdmins)
                {
                    while (index < ev.Sessions.Count && ev.Sessions[index].SessionId < admin.sessionId)
                        index++;
                    if (index < ev.Sessions.Count && admin.sessionId == ev.Sessions[index].SessionId)
                        ev.Sessions[index].SessionAdmins.Add(Models.User.Hydrate(admin));
                }
                return Ok(ev);
            }
        }

        /// <summary>
        /// List All events
        /// </summary>
        /// <remarks>Gets a list of all &#x60;event&#x60; entities.</remarks>
        /// <response code="200">Successful response - returns an array of &#x60;event&#x60; entities.</response>
        [HttpGet]
        [ValidateModelState]
        [SwaggerOperation("GetEvents")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Event>), description: "Successful response - returns an array of &#x60;event&#x60; entities.")]
        public async Task<IActionResult> GetEvents([FromQuery] DateTime? startDate)
        {
            //startDate = startDate ?? DateTime.Now;
            // Rather than join the events to sessions to admins--which results in a table of size # sessions * # admins per event--it's more efficient
            //   just to fetch the sessions & admins separately and add them to each event.
//            string sql =
//                "SELECT * FROM Events WHERE start > @startDate ORDER BY eventId;"
//                + "SELECT * FROM Sessions WHERE eventId IN (SELECT eventId FROM Events WHERE start > @startDate) ORDER BY eventId;"
//                + "SELECT * FROM EventAdmins INNER JOIN Users ON EventAdmins.userId = Users.userId WHERE eventId IN (SELECT eventId FROM Events WHERE start < @startDate) ORDER BY eventId;";
            string sql =
                @"SELECT * FROM Events ORDER BY eventId;
                  SELECT * FROM Sessions WHERE eventId IN (SELECT eventId FROM Events) ORDER BY eventId;
                  SELECT * FROM EventAdmins INNER JOIN Users ON EventAdmins.userId = Users.userId ORDER BY eventId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
//                var result = await conn.QueryMultipleAsync(sql, new { startDate = startDate });
                var result = await conn.QueryMultipleAsync(sql, new { startDate = startDate });
                // Since the sessions and admins will be returned in-order, we can optimize the process of adding them to events
                //   by iterating over events in-order rather than having to search the array of events every time.
                var events = result.Read<Event>().Distinct().ToArray();
                foreach (var ev in events)
                {
                    ev.Sessions = new List<Session>();
                    ev.EventAdmins = new List<User>();
                }
                var sessions = result.Read<Session>().Distinct().ToList();
                var index = 0;
                foreach (var session in sessions)
                {
                    session.Rsvps = new List<Rsvp>();
                    session.SessionAdmins = new List<User>();
                    while (index < events.Length && events[index].EventId < session.EventId)
                        index++;
                    if (index < events.Length && events[index].EventId == session.EventId)
                        events[index].Sessions.Add(session);
                }
                dynamic admins = result.Read().Distinct().ToList();
                index = 0;
                foreach (var admin in admins)
                {
                    while (index < events.Length && admin.eventId < events[index].EventId)
                        index++;
                    if (index < events.Length && admin.eventId == events[index].EventId)
                        events[index].EventAdmins.Add(Models.User.Hydrate(admin));
                }
                return Ok(events);
            }
        }

        /// <summary>
        /// Update a event
        /// </summary>
        /// <remarks>Updates an existing &#x60;event&#x60;.</remarks>
        /// <param name="eventId">A unique identifier for a &#x60;event&#x60;.</param>
        /// <param name="ev">Updated &#x60;event&#x60; information.</param>
        /// <response code="202">Successful response.</response>
        [HttpPut("{eventId}")]
        [ValidateModelState]
        [SwaggerOperation("UpdateEvent")]
        public async Task<IActionResult> UpdateEvent([FromRoute][Required] int eventId, [FromBody] Event ev)
        {
            Console.WriteLine(ev);
            ev.EventId = eventId;
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_UPDATE.ToFriendlyString(), "event", ev);
            if (!await auth.IsEventAdmin(UserId, ev.EventId.Value))
                return Unauthorized();
            string sql =
                 @"UPDATE Events SET name = @name, description = @description, image = @image, start = @start,
                     end = @end, checkinWindow = @checkinWindow WHERE eventId = @eventId;
                   SELECT * FROM Events WHERE eventId = @eventId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql,
                    new {eventId, name = ev.Name, description = ev.Description, image = ev.Image, start = ev.Start, end = ev.End, checkinWindow = ev.CheckinWindow});
                if (result.Any())
                {
                    var updatedEvent = result.FirstOrDefault();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.UPDATED.ToFriendlyString(), "event", updatedEvent);
                    return AcceptedAtRoute("GetEvent", new { eventId }, updatedEvent);
                }
				else
                    return StatusCode(500);
            }
        }

    }
}
