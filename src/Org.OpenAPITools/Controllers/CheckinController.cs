using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{
    [Route("api/checkins")]
    [Authorize]
    public class CheckinController : ControllerBase
    {
        private DatabaseSettings db;
        private IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public CheckinController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }
        
        public class CheckinPostModel
        {
            public string userId;
            public int sessionId;
            public DateTime? inTime;
        }

        /// <summary>
        /// Create a Checkin
        /// </summary>
        /// <remarks>Creates a new instance of a &#x60;Checkin&#x60;.</remarks>
        /// <param name="userId">The user for which to add a checkin</param>
        /// <param name="sessionId">The session for which to add a checkin</param>
        /// <param name="inTime">The time of the checkin</param>
        /// <response code="201">Successful response.</response>
        [HttpPost]
        [ValidateModelState]
        [SwaggerOperation("CreateCheckin")]
        [SwaggerResponse(statusCode: 201, type: typeof(Checkin), description: "Successful response.")]
        public async Task<IActionResult> CreateCheckin([FromBody] CheckinPostModel model)
        {
            var currentTime = DateTime.Now;
            string sql =
                @"SELECT Sessions.start, checkinWindow FROM Events INNER JOIN Sessions
                    ON Events.eventId = Sessions.eventId WHERE sessionId = @sessionId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "checkin", model);
                conn.Open();
                var isSessionAdminTask = auth.IsSessionAdmin(UserId, model.sessionId);
                var sessionInfoTask = conn.QueryAsync(sql, new {model.sessionId});
                var isSessionAdmin = await isSessionAdminTask;
                var sessionInfo = (await sessionInfoTask).FirstOrDefault();
                if (sessionInfo == null)
                    return BadRequest();
                if (!isSessionAdmin || !model.inTime.HasValue)
                    model.inTime = currentTime;
                if (!isSessionAdmin && (model.userId != UserId ||
                                        model.inTime < sessionInfo.start.AddMinutes(-sessionInfo.checkinWindow) ||
                                        model.inTime > sessionInfo.start.AddMinutes(sessionInfo.checkinWindow)))
                    return Unauthorized();
                sql =
                    @"INSERT INTO Checkins (sessionId, userId, inTime) VALUES (@sessionId, @userId, @inTime);
                      SELECT * FROM Checkins WHERE checkinId = LAST_INSERT_ID();";
                var result = await conn.QueryAsync(sql, new
                {
                    model.sessionId,
                    model.userId,
                    model.inTime
                });
                if (result.Any())
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "checkin", model);
                    var newCheckin = result.First();
                    return CreatedAtRoute("GetCheckin", new { checkinId = newCheckin.checkinId }, newCheckin);
                }
                else
                    return UnprocessableEntity();
            }
        }

        /// <summary>
        /// Delete a Checkin
        /// </summary>
        /// <remarks>Deletes an existing &#x60;Checkin&#x60;.</remarks>
        /// <param name="checkinId">A unique identifier for a &#x60;Checkin&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("{checkinId}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteCheckin")]
        public async Task<IActionResult> DeleteCheckin([FromRoute][Required] int checkinId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "checkin", new {checkinId});
            string sql = "SELECT * FROM Checkins WHERE checkinId = @checkinId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {checkinId});
                var c = result.FirstOrDefault();
                if (c == null)
                    return StatusCode(500);
                if (!await auth.IsSessionAdmin(UserId, c.sessionId) &&
                    c.userId != UserId)
                    return Unauthorized();
                sql = "DELETE FROM Checkins WHERE checkinId = @checkinId";
                var deleteResult = await conn.ExecuteAsync(sql, new { checkinId });
                if (deleteResult > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "checkin", new {checkinId});
                    return StatusCode(204);
                }
                else
                    return StatusCode(500);
            }
        }
        
        /// <summary>
        /// Get a Checkin
        /// </summary>
        /// <remarks>Gets the details of a single instance of a &#x60;Checkin&#x60;.</remarks>
        /// <param name="checkinId">A unique identifier for a &#x60;Checkin&#x60;.</param>
        /// <response code="200">Successful response - returns a single &#x60;Checkin&#x60;.</response>
        [HttpGet("{checkinId}", Name = "GetCheckin")]
        [ValidateModelState]
        [SwaggerOperation("GetCheckin")]
        [SwaggerResponse(statusCode: 200, type: typeof(Checkin), description: "Successful response - returns a single &#x60;Checkin&#x60;.")]
        public async Task<IActionResult> GetCheckin([FromRoute][Required] int checkinId)
        {
            string sql =
                @"SELECT * FROM Checkins WHERE checkinId = @checkinId;
                  SELECT * FROM Users JOIN Checkins using (userId) where Checkins.checkinId = @checkinId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new { checkinId });
                var checkin = result.ReadFirstOrDefault<Checkin>();
                if (checkin == null)
                    return NotFound();
                checkin.User = result.ReadSingle<User>();

                return Ok(checkin);
            }
        }
    }
}