using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using MySql.Data.MySqlClient;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{
    [Route("api/notify")]
    [Authorize]
    public class NotificationController : ControllerBase
    {
        private readonly DatabaseSettings db;
        private readonly EmailSettings emailSettings;
        private readonly IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public NotificationController(IOptions<DatabaseSettings> databaseSettings, IOptions<EmailSettings> emailSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            this.emailSettings = emailSettings.Value;
            auth = authorizationSettings;
        }

        /// <summary>
        ///     Notify all users in the system
        /// </summary>
        /// <param name="notification">The notification to send.</param>
        /// <response code="204">Successful response.</response>
        [HttpPost("all")]
        [ValidateModelState]
        [SwaggerOperation("NotifyAll")]
        public async Task<IActionResult> NotifyAll([FromBody] Notification notification)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "system notification", notification);
            var isAdmin = await auth.IsSuperAdmin(UserId);
            if (!isAdmin)
                return Unauthorized();
            
            var sql = "SELECT email FROM users";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<string>(sql);
                SendEmail(result, notification);
                log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "system notification", new { emails = result, notification});
                return StatusCode(204);
            }
        }

        /// <summary>
        ///     Notify attendees of an event
        /// </summary>
        /// <param name="eventId">The event for which to notify attendees.</param>
        /// <param name="notification">The notification to send.</param>
        /// <response code="204">Successful response.</response>
        [HttpPost("event/{eventId}")]
        [ValidateModelState]
        [SwaggerOperation("NotifyEvent")]
        public async Task<IActionResult> NotifyEvent([FromRoute] int eventId, [FromBody] Notification notification)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "event notification", new {eventId, notification});
            var isAdmin = await auth.IsEventAdmin(UserId, eventId);
            if (!isAdmin)
                return Unauthorized();
            
            var sql =
                @"SELECT DISTINCT email FROM sessions s
                    LEFT JOIN rsvps r on s.sessionId = r.sessionId
                    LEFT JOIN users u on r.userId = u.userId
                    WHERE eventId = @eventId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<string>(sql, new {eventId});
                SendEmail(result, notification);
                log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "system notification", new { emails = result, notification});
                return StatusCode(204);
            }
        }

        /// <summary>
        ///     Notify attendees of a session
        /// </summary>
        /// <param name="sessionId">The session for which to notify attendees.</param>
        /// <param name="notification">The notification to send.</param>
        /// <response code="204">Successful response.</response>
        [HttpPost("session/{sessionId}")]
        [ValidateModelState]
        [SwaggerOperation("NotifySession")]
        public async Task<IActionResult> NotifySession([FromRoute] int sessionId, [FromBody] Notification notification)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "session notification", new {sessionId, notification});
            var isAdmin = await auth.IsSessionAdmin(UserId, sessionId);
            if (!isAdmin)
                return Unauthorized();
            
            var sql =
                @"SELECT email FROM rsvps r
                    LEFT JOIN users u on r.userId = u.userId
                    WHERE sessionId = @sessionId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<string>(sql, new {sessionId});
                SendEmail(result, notification);
                log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "system notification", new { emails = result, notification});
                return StatusCode(204);
            }
        }

		/// <summary>
        /// Sends an email
        /// </summary>
        /// <remarks>Sends a notification to a &#x60;user&#x60; using his or her email address</remarks>
        /// <param name="to">A &#x60;IEnumerable&#x60; containing the address of the notification's recipient</param>
		/// <param name="to">A &#x60;Notification&#x60; containing the information to be sent</param>
        private async void SendEmail(IEnumerable<string> to, Notification notification)
        {
            var email = new MimeMessage();
            email.Bcc.AddRange(to
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Select(s => new MailboxAddress(s))
            );
            email.From.Add(new MailboxAddress(emailSettings.SmtpUsername));
            email.Subject = notification.title;
            email.Body = new TextPart(TextFormat.Plain)
            {
                Text = notification.body
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(emailSettings.SmtpServer, emailSettings.SmtpPort, true);

                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(emailSettings.SmtpUsername, emailSettings.SmtpPassword);

                await client.SendAsync(email);
                await client.DisconnectAsync(true);
            }
        }
    }
}