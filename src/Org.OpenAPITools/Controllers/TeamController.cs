using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Update;
using Microsoft.Extensions.Options;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{
    
    [Route("api/teams")]
    [Authorize]
    public class TeamController : ControllerBase
    {

        private DatabaseSettings db;
        private IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public TeamController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }
        
        /// <summary>
        /// Create a team
        /// </summary>
        /// <remarks>Creates a new instance of a &#x60;team&#x60;.</remarks>
        /// <param name="team">A new &#x60;team&#x60; to be created.</param>
        /// <response code="201">Successful response.</response>
        [HttpPost]
        [ValidateModelState]
        [SwaggerOperation("CreateTeam")]
        [SwaggerResponse(statusCode: 201, type: typeof(Team), description: "Successful response.")]
        public async Task<IActionResult> CreateTeam([FromBody]Team team)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "team", team);
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql =
                @"INSERT INTO Teams (name, teamAdmin) VALUES (@name, @teamAdmin);
                  SELECT * FROM Teams WHERE teamId = LAST_INSERT_ID();";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {name = team.Name, teamAdmin = team.TeamAdmin.UserId});
                if (result.Any())
                {
                    var newTeam = result.FirstOrDefault();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "team", newTeam);
                    return CreatedAtRoute("GetTeam", new {teamId = newTeam.teamId}, newTeam);
                }
                else
                    return UnprocessableEntity();
            }
        }

        /// <summary>
        /// Delete a team
        /// </summary>
        /// <remarks>Deletes an existing &#x60;team&#x60;.</remarks>
        /// <param name="teamId">A unique identifier for a &#x60;team&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("{teamId}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteTeam")]
        public async Task<IActionResult> DeleteTeam([FromRoute][Required] int teamId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "team", new { teamId });

            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql = "DELETE FROM Teams WHERE teamId = @teamId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {teamId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "team", new { teamId });
                    return NoContent();
                }
                else
                    return NotFound();
            }
        }

        /// <summary>
        /// Get a team
        /// </summary>
        /// <remarks>Gets the details of a single instance of a &#x60;team&#x60;.</remarks>
        /// <param name="teamId">A unique identifier for a &#x60;team&#x60;.</param>
        /// <response code="200">Successful response - returns a single &#x60;team&#x60;.</response>
        [HttpGet("{teamId}", Name = "GetTeam")]
        [ValidateModelState]
        [SwaggerOperation("GetTeam")]
        [SwaggerResponse(statusCode: 200, type: typeof(Team), description: "Successful response - returns a single &#x60;team&#x60;.")]
        public async Task<IActionResult> GetTeam([FromRoute][Required] int teamId)
        {
            string sql =
                @"SELECT teamId, name FROM Teams INNER JOIN Users ON teamAdmin = Users.userId WHERE teamId = @teamId;
                  SELECT * FROM Users WHERE userId = (SELECT teamAdmin FROM Teams WHERE teamId = @teamId);
                  SELECT * FROM TeamMembers INNER JOIN Users ON TeamMembers.userId = Users.userId WHERE TeamMembers.teamId = @teamId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new {teamId});
                var ev = result.ReadSingle<Team>();
                ev.TeamAdmin = result.ReadSingleOrDefault<User>();
                ev.Members = result.Read<User>().Distinct().ToList();
                return Ok(ev);
            }
        }
        
        /// <summary>
        /// List All teams
        /// </summary>
        /// <remarks>Gets a list of all &#x60;team&#x60; entities.</remarks>
        /// <response code="200">Successful response - returns an array of &#x60;team&#x60; entities.</response>
        [HttpGet]
        [ValidateModelState]
        [SwaggerOperation("GetTeams")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Team>), description: "Successful response - returns an array of &#x60;team&#x60; entities.")]
        public async Task<IActionResult> GetTeams()
        {
            string sql =
                @"SELECT teamId, name, userId, firstName, lastName, email, profileIcon FROM Teams
                    JOIN Users ON Teams.teamAdmin = Users.userId ORDER BY teamId;
                  SELECT * FROM TeamMembers JOIN Users ON TeamMembers.userId = Users.userId ORDER BY teamId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql);
                var teams = result.Read<Team, User, Team>((team, user) =>
                {
                    team.TeamAdmin = user;
                    team.Members = new List<User>();
                    return team;
                }, "userId").Distinct().ToArray();
                var count = 0;
                var members = result.Read().Distinct().ToList();
                foreach (var m in members)
                {
                    while (count < teams.Length && teams[count].TeamId < m.teamId)
                        count++;
                    if (count < teams.Length && teams[count].TeamId == m.teamId)
                        teams[count].Members.Add(Models.User.Hydrate(m));
                }
                return Ok(teams);
            }
        }
        
        /// <summary>
        /// Update a team
        /// </summary>
        /// <remarks>Updates an existing &#x60;team&#x60;.</remarks>
        /// <param name="teamId">A unique identifier for a &#x60;team&#x60;.</param>
        /// <param name="team">Updated &#x60;team&#x60; information.</param>
        /// <response code="202">Successful response.</response>
        [HttpPut("{teamId}")]
        [ValidateModelState]
        [SwaggerOperation("UpdateTeam")]
        public async Task<IActionResult> UpdateTeam([FromRoute][Required] int teamId, [FromBody] Team team)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_UPDATE.ToFriendlyString(), "team", team);
            if (!await auth.IsTeamAdmin(UserId, teamId))
                return Unauthorized();
            string sql =
                @"UPDATE Teams SET name = @name WHERE teamId = @teamId;
                    SELECT * FROM Teams WHERE teamId = @teamId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql,
                    new {teamId, name = team.Name});
                if (result.Any())
                {
                    var newTeam = result.FirstOrDefault();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.UPDATED.ToFriendlyString(), "team", newTeam);
                    return AcceptedAtRoute("GetTeam", new { teamId }, newTeam);
                }
				else
                    return StatusCode(500);
            }
        }

        /// <summary>
        /// Add a team member
        /// </summary>
        /// <remarks>Add an existing &#x60;user&#x60; as a new team member.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <param name="teamId">A unique identifier for a &#x60;team&#x60;.</param>
        /// <response code="202">Successful response.</response>
        [HttpPost("{teamId}/members/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> AddTeamMember([FromRoute][Required] int teamId, [FromRoute][Required] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_UPDATE.ToFriendlyString(), "user", userId, "team member", new {teamId, userId});
            if (!await auth.IsTeamAdmin(UserId, teamId))
                return Unauthorized();
            string sql = "INSERT INTO TeamMembers (teamId, userId) VALUES (@teamId, @userId);";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {teamId, userId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.UPDATED.ToFriendlyString(), "user", userId, "as member of team", new {teamId, userId});
                    return Accepted();
                }
                else
                    return BadRequest();
            }
        }

        /// <summary>
        /// Delete a team member
        /// </summary>
        /// <remarks>Delete an existing &#x60;user&#x60; as a team member.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <param name="teamId">A unique identifier for a &#x60;team&#x60;.</param>
        /// <response code="202">Successful response.</response>
        [HttpDelete("{teamId}/members/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> DeleteTeamMember([FromRoute][Required] int teamId, [FromRoute][Required] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "user", userId, "team member", new {userId, teamId});
            if (!await auth.IsTeamAdmin(UserId, teamId))
                return Unauthorized();
            string sql = "DELETE FROM TeamMembers WHERE teamId = @teamId AND userId = @userId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {teamId, userId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "user", userId, "team member", new {userId, teamId});
                    return NoContent();
                }
                else
                    return NotFound();
            }
        }

        /// <summary>
        /// Update the team admin
        /// </summary>
        /// <remarks>Set an existing &#x60;user&#x60; as the team admin.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <param name="teamId">A unique identifier for a &#x60;team&#x60;.</param>
        /// <response code="202">Successful response.</response>
        [HttpPut("{teamId}/admins/{userId}")]
        [ValidateModelState]
        public async Task<IActionResult> UpdateTeamAdmin([FromRoute][Required] int teamId, [FromRoute][Required] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_UPDATE.ToFriendlyString(), "user", userId, "team admin", new {userId, teamId});
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql = "UPDATE Teams SET teamAdmin = @userId WHERE teamId = @teamId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {teamId, userId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.UPDATED.ToFriendlyString(), "user", userId, "team admin", new {userId, teamId});
                    return Accepted();
                }
                else
                    return NotFound();
            }
        }
    }
}