using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Org.OpenAPITools.Controllers
{
    
    [Authorize]
    public class SessionController : ControllerBase
    {
        
        private DatabaseSettings db;
        private IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public SessionController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }

        /// <summary>
        /// Create a session
        /// </summary>
        /// <remarks>Creates a new instance of a &#x60;session&#x60;.</remarks>
        /// <param name="session">A new &#x60;session&#x60; to be created.</param>
        /// <response code="201">Successful response.</response>
        [HttpPost("api/events/{eventId}/sessions")]
        [ValidateModelState]
        [SwaggerOperation("CreateSession")]
        [SwaggerResponse(statusCode: 201, type: typeof(Session), description: "Successful response.")]
        public async Task<IActionResult> CreateSession([FromRoute][Required] int eventId, [FromBody] Session session)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "session", session);
            if (!await auth.IsEventAdmin(UserId, eventId))
                return Unauthorized();
            string sql =
                @"INSERT INTO Sessions (name, description, image, location, start, end, eventId)
                    VALUES (@name, @description, @image, @location, @start, @end, @eventId);
                  SELECT * FROM Sessions WHERE sessionId = LAST_INSERT_ID();";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new
                {
                    eventId, name = session.Name, description = session.Description, image = session.Image,
                    location = session.Location, start = session.Start, end = session.End
                });
                var newSession = result.FirstOrDefault();
                log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED, "session", newSession);
                return CreatedAtRoute("GetSession", new {sessionId = newSession.sessionId}, newSession);
            }
        }

        /// <summary>
        /// Delete a session
        /// </summary>
        /// <remarks>Deletes an existing &#x60;session&#x60;.</remarks>
        /// <param name="sessionId">A unique identifier for a &#x60;session&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("api/sessions/{sessionId}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteSession")]
        public async Task<IActionResult> DeleteSession([FromRoute][Required] int sessionId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "session", new {sessionId});
            if (!await auth.IsEventAdminForSession(UserId, sessionId))
                return Unauthorized();
            string sql = "DELETE FROM Sessions WHERE sessionId = @sessionId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {sessionId});
                if (result > 0) {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "session", new {sessionId});
                    return Ok();
                }
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Get a session
        /// </summary>
        /// <remarks>Gets the details of a single instance of a &#x60;session&#x60;.</remarks>
        /// <param name="sessionId">A unique identifier for a &#x60;session&#x60;.</param>
        /// <response code="200">Successful response - returns a single &#x60;session&#x60;.</response>
        [HttpGet("api/sessions/{sessionId}", Name = "GetSession")]
        [ValidateModelState]
        [SwaggerOperation("GetSession")]
        [SwaggerResponse(statusCode: 200, type: typeof(Session), description: "Successful response - returns a single &#x60;session&#x60;.")]
        public async Task<IActionResult> GetSession([FromRoute][Required] int sessionId)
        {
            string sql =
                @"SELECT * FROM Sessions WHERE sessionId = @sessionId;
                  SELECT * FROM Rsvps INNER JOIN Users ON Rsvps.userId = Users.userId WHERE sessionId = @sessionId;
                  SELECT * FROM SessionAdmins INNER JOIN Users ON SessionAdmins.userId = Users.userId WHERE sessionId = @sessionId;
                  SELECT * FROM Checkins INNER JOIN Users ON Checkins.userId = Users.userId WHERE sessionId = @sessionId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new {sessionId});
                var session = result.ReadFirstOrDefault<Session>();
                if (session == null)
                    return NotFound();
                session.Rsvps = result.Read<Rsvp,User,Rsvp>(
                    (rsvp, user) =>
                    {
                        rsvp.User = user;
                        return rsvp;
                    },
                    splitOn: "firstName")
                    .Distinct()
                    .ToList();
                session.SessionAdmins = result.Read<User>().Distinct().ToList();
                session.Checkins = result.Read<Checkin,User,Checkin>(
                    (checkin, user) =>
                    {
                        checkin.User = user;
                        return checkin;
                    },
                    splitOn: "firstName")
                    .Distinct()
                    .ToList();
                return Ok(session);
            }
        }

        [Obsolete("To get the sessions for an event, call getEvent for the event in question.")]
        [HttpGet]
        [Route("api/events/{eventId}/sessions")]
        public async Task<IActionResult> GetEventSessions([FromRoute][Required] int eventId)
        {
            string sql = "SELECT * FROM Sessions WHERE eventId = @eventId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {eventId});
                return Ok(result.Distinct().ToList());
            }
        }
        
        /// <summary>
        /// Update a session
        /// </summary>
        /// <remarks>Updates an existing &#x60;session&#x60;.</remarks>
        /// <param name="sessionId">A unique identifier for a &#x60;session&#x60;.</param>
        /// <param name="session">Updated &#x60;session&#x60; information.</param>
        /// <response code="202">Successful response.</response>
        [HttpPut("api/sessions/{sessionId}")]
        [ValidateModelState]
        [SwaggerOperation("UpdateSession")]
        public async Task<IActionResult> UpdateSession([FromRoute][Required] int sessionId, [FromBody] Session session)
        {
            session.SessionId = sessionId;
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_UPDATE.ToFriendlyString(), "session", session);
            if (!await auth.IsSessionAdmin(UserId, sessionId))
                return Unauthorized();
            string sql =
                @"UPDATE Sessions SET name = @name, description = @description, image = @image,
                    location = @location, start = @start, end = @end WHERE sessionId = @sessionId;
                  SELECT * FROM Sessions WHERE sessionId = @sessionId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                session.SessionId = sessionId;
                var result = await conn.QueryAsync(sql, session);
                if (result.Any()) {
                    var newSession = result.FirstOrDefault();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.UPDATED.ToFriendlyString(), "session", newSession);
                    return AcceptedAtRoute("GetSession", new {sessionId}, newSession);
                }
                else
                    return StatusCode(500);
            }
        }
    }
}