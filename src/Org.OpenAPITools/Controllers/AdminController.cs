using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;


namespace Org.OpenAPITools.Controllers
{
    [Authorize]
    [Route("api")]
    public class AdminController : ControllerBase
    {
        private DatabaseSettings db;
        private IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public AdminController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }
		
		/// <summary>
        /// Create a admin
        /// </summary>
        /// <remarks>Creates a new instance of a &#x60;admin&#x60;.</remarks>
        /// <param name="admin">A new &#x60;admin&#x60; to be created.</param>
        /// <response code="201">Successful response.</response>
        [Authorize]
        [HttpPost("admins/{userId}")]
        public async Task<IActionResult> CreateSuperAdmin([Required][FromRoute] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "system admin", new {userId});
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql =
                @"INSERT INTO Admins (userId) SELECT @userId
                    WHERE NOT EXISTS (SELECT userId FROM Admins WHERE userId = @userId);
                  SELECT * FROM Admins WHERE adminId = LAST_INSERT_ID()";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {userId});
                // Replace with Created() when we create appropriate endpoints.
                if (EnumerableExtensions.Any(result))
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "system admin", result.First());
                    return StatusCode(201);
                }
                else
                    return StatusCode(500);
            }

            
        }

		/// <summary>
        /// Delete a admin
        /// </summary>
        /// <remarks>Deletes an existing &#x60;admin&#x60;.</remarks>
        /// <param name="adminId">A unique identifier for a &#x60;admin&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("admins/{userId}")]
        public async Task<IActionResult> DeleteSuperAdmin([Required][FromRoute] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "system admin", new {userId});
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql = "DELETE FROM Admins WHERE userId = @userId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {userId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "system admin", new {userId});
                    return NoContent();
                }
                else
                    return NotFound();
            }
        }

		/// <summary>
        /// Determine if an admin is a super admin
        /// </summary>
        /// <remarks>Tests a &#x60;admin&#x60; to determine status as super admin</remarks>
        /// <param name="userId">ID of an &#x60;admin&#x60; to be tested</param>
        [HttpGet("admins/{userId}")]
        public async Task<IActionResult> IsSuperAdmin([Required] [FromRoute] string userId)
        {
            return Ok(await auth.IsSuperAdmin(userId));
        }
        
		/// <summary>
        /// Create an event admin
        /// </summary>
        /// <remarks>Creates a new instance of an event &#x60;admin&#x60;.</remarks>
        /// <param name="admin">A new &#x60;admin&#x60; to be created.</param>
        /// <response code="201">Successful response.</response>
        [HttpPost("events/{eventId}/admins/{userId}")]
        public async Task<IActionResult> CreateEventAdmin([Required][FromRoute] int eventId, [Required][FromRoute] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "event admin", new {eventId, userId});
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql =
                @"INSERT INTO EventAdmins (eventId, userId) SELECT @eventId, @userId 
                    WHERE NOT EXISTS (SELECT eventId, userId FROM EventAdmins WHERE eventId = @eventId AND userId = @userId);
                  SELECT * FROM EventAdmins WHERE eventAdminId = LAST_INSERT_ID()";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {eventId, userId});
                var newEventAdmin = result.FirstOrDefault();
                if (newEventAdmin != null)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "event admin", newEventAdmin);
                    return StatusCode(201);
                }
                else
                    return StatusCode(500);
            }
        }
        
		/// <summary>
        /// Delete an event admin
        /// </summary>
        /// <remarks>Deletes an existing event &#x60;admin&#x60;.</remarks>
        /// <param name="adminId">A unique identifier for a &#x60;admin&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("events/{eventId}/admins/{userId}")]
        public async Task<IActionResult> DeleteEventAdmin([Required][FromRoute] int eventId, [Required][FromRoute] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "event admin", new {eventId, userId});
            if (!await auth.IsSuperAdmin(UserId))
                return Unauthorized();
            string sql = "DELETE FROM EventAdmins WHERE eventId = @eventId AND userId = @userId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {eventId, userId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "event admin", new {eventId, userId});
                    return NoContent();
                }
                else
                    return NotFound();
            }
        }

		/// <summary>
        /// Determine if an admin is an event admin
        /// </summary>
        /// <remarks>Test a &#x60;admin&#x60; to determine status as event admin</remarks>
        /// <param name="userId">ID of an &#x60;admin&#x60; to be tested</param>
		/// <param name="eventId">ID of an &#x60;event&#x60; to be tested</param>
        [HttpGet("events/{eventId}/admins/{userId}")]
        public async Task<IActionResult> IsEventAdmin([Required][FromRoute] int eventId, [Required][FromRoute] string userId)
        {
            return Ok(await auth.IsEventAdmin(userId, eventId));
        }

		/// <summary>
        /// Create a session admin
        /// </summary>
        /// <remarks>Creates a new instance of a session &#x60;admin&#x60;.</remarks>
        /// <param name="admin">A new &#x60;admin&#x60; to be created.</param>
        /// <response code="201">Successful response.</response>
        [HttpPost("sessions/{sessionId}/admins/{userId}")]
        public async Task<IActionResult> CreateSessionAdmin([Required][FromRoute] int sessionId, [Required][FromRoute] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "session admin", new {sessionId, userId});
            if (!await auth.IsEventAdminForSession(UserId, sessionId))
                return Unauthorized();
            string sql =
                @"INSERT INTO SessionAdmins (sessionId, userId) SELECT @sessionId, @userId
                    WHERE NOT EXISTS (SELECT sessionId, userid FROM SessionAdmins WHERE sessionId = @sessionId AND userId = @userId);
                  SELECT * FROM SessionAdmins WHERE sessionAdminId = LAST_INSERT_ID()";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {sessionId, userId});
                var newSessionAdmin = result.FirstOrDefault();
                if (newSessionAdmin != null)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "session admin", newSessionAdmin);
                    return StatusCode(201);
                }
                else
                    return StatusCode(500);
            }
        }

		/// <summary>
        /// Delete a session admin
        /// </summary>
        /// <remarks>Deletes an existing session &#x60;admin&#x60;.</remarks>
        /// <param name="adminId">A unique identifier for a &#x60;admin&#x60;.</param>
        /// <response code="204">Successful response.</response>
        [HttpDelete("sessions/{sessionId}/admins/{userId}")]
        public async Task<IActionResult> DeleteSessionAdmin([Required][FromRoute] int sessionId, [Required][FromRoute] string userId)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_DELETE.ToFriendlyString(), "session admin", new {sessionId, userId});
            if (!await auth.IsEventAdminForSession(UserId, sessionId))
                return Unauthorized();
            string sql = "DELETE FROM SessionAdmins WHERE sessionId = @sessionId AND userId = @userId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.ExecuteAsync(sql, new {sessionId, userId});
                if (result > 0)
                {
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.DELETED.ToFriendlyString(), "session admin", new {sessionId, userId});
                    return NoContent();
                }
                else
                    return NotFound();
            }
        }

		/// <summary>
        /// Determines whether an admin is a session admin
        /// </summary>
        /// <remarks>Tests an &#x60;admin&#x60; to determine status as session admin</remarks>
        /// <param name="userId">ID of an &#x60;admin&#x60; to be tested</param>
		/// <param name="sessionId">ID of an &#x60;session&#x60; to be tested</param>
        [HttpGet("sessions/{sessionId}/admins/{userId}")]
        public async Task<IActionResult> IsSessionAdmin([Required][FromRoute] int sessionId, [Required][FromRoute] string userId)
        {
            return Ok(await auth.IsSessionAdmin(userId, sessionId));
        }
    }
}