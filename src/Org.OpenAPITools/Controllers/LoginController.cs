using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Org.OpenAPITools.Models;

namespace Org.OpenAPITools.Controllers
{
    public class LoginController : ControllerBase
    {
        private readonly DatabaseSettings db;
        private readonly OpenIdSettings openId;
        private string UserId => Util.GetUserId(User);
        
        public LoginController(IOptions<DatabaseSettings> databaseSettings, IOptions<OpenIdSettings> openIdSettings)
        {
            db = databaseSettings.Value;
            openId = openIdSettings.Value;
        }
        
		/// <summary>
        /// Attempts to log a user in via Oauth
        /// </summary>
        /// <remarks>Logs a &#x60;user&#x60; into the Eventir system</remarks>
        [Route("api/login")]
        [HttpGet]
        public async Task<IActionResult> Login()
        {
            if (AuthenticationHttpContextExtensions.AuthenticateAsync(HttpContext).Result.Succeeded)
            {
                User user = ParseUserFromClaims();

                string sql = "SELECT * FROM Users WHERE userId = @userId";
                using (IDbConnection conn = new MySqlConnection(db.MySQLString))
                {
                    conn.Open();
                    var result = await conn.QueryAsync<User>(sql, user);
                    var existingUser = result.FirstOrDefault();
                    if (existingUser == null)
                    {
                        sql =
                            @"INSERT INTO Users (userId, firstName, lastName, profileIcon, email)
                                VALUES (@userId, @firstName, @lastName, @profileIcon, @email)";
                        await conn.ExecuteAsync(sql, user);
                    }
                    else if (existingUser.FirstName != user.FirstName || existingUser.LastName != user.LastName ||
                             existingUser.Email != user.Email)
                    {
                        sql = "UPDATE Users SET firstName = @firstName, lastName = @lastName, email = @email WHERE userId = @userId";
                        await conn.ExecuteAsync(sql, user);
                    }
                }

                if (Request.Query.ContainsKey("ReturnUrl"))
                    return Redirect(Request.Query["ReturnUrl"]);
                else
                    return Redirect(openId.HomeUrl);
            }
            else
                return Challenge("Google");
        }

		/// <summary>
        /// Attempts to get information of users currently logged into Eventir
        /// </summary>
        /// <remarks>Gets the &#x60;user&#x60; info for all users logged in to Eventir</remarks>
        [Route("api/users/current")]
        [HttpGet]
        public async Task<IActionResult> GetCurrentUserInfo()
        {
            if (!AuthenticationHttpContextExtensions.AuthenticateAsync(HttpContext).Result.Succeeded)
            {
                return Unauthorized();
            }

            var userId = UserId;
            var sql = "SELECT * FROM Users WHERE userId = @userId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<User>(sql, new {userId});
                return Ok(result.FirstOrDefault());
            }
        }
		
		/// <summary>
        /// Attempt to get user claims
        /// </summary>
        /// <remarks>Returns claims of every &#x60;user&#x60;</remarks>
        private Dictionary<string, string> GetUserClaims()
        {
            var claims = new Dictionary<string, string>();
            foreach (var claim in HttpContext.User.Claims)
            {
                claims.Add(claim.Type, claim.Value);
            }
            return claims;
        }

		/// <summary>
        /// Returns a user
        /// </summary>
        /// <remarks>Returns a &#x60;user&#x60; with properties deriving from user claims</remarks>
        private User ParseUserFromClaims()
        {
            var claims = GetUserClaims();
            return new User()
            {
                Email = claims["email"],
                ProfileIcon = claims["picture"],
                FirstName = claims["given_name"],
                LastName = claims["family_name"],
                UserId = claims["sub"]
            };
        }

		/// <summary>
        /// Logs out a user
        /// </summary>
        /// <remarks>Logs out a logged-in &#x60;user&#x60; from Eventir</remarks>
        [Route("api/logout")]
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            if (AuthenticationHttpContextExtensions.AuthenticateAsync(HttpContext).Result.Succeeded)
            {
                Response.Cookies.Delete($".AspNetCore.{CookieAuthenticationDefaults.AuthenticationScheme}");
                return Redirect(openId.LoginUrl);
            }
            else
                return Unauthorized();
        }
    }
}