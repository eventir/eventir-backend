using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Org.OpenAPITools.Attributes;
using Org.OpenAPITools.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace Org.OpenAPITools.Controllers
{

    [Route("api/users")]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly DatabaseSettings db;
        private readonly IAuthorization auth;
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public UserController(IOptions<DatabaseSettings> databaseSettings, IAuthorization authorizationSettings)
        {
            db = databaseSettings.Value;
            auth = authorizationSettings;
        }

		/// <summary>
        /// Get a user
        /// </summary>
        /// <remarks>Gets the details of a single instance of a &#x60;user&#x60;.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <response code="200">Successful response - returns a single &#x60;user&#x60;.</response>
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUser([FromRoute] [Required] string userId)
        {
            string sql = "SELECT * FROM Users WHERE userId = @userId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<User>(sql, new { userId });
                if (result.Any())
                    return Ok(result.FirstOrDefault());
                else
                    return NotFound();
            }
        }

		/// <summary>
        /// List All users
        /// </summary>
        /// <remarks>Gets a list of all &#x60;user&#x60; entities.</remarks>
        /// <response code="200">Successful response - returns an array of &#x60;user&#x60; entities.</response>
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            string sql = "SELECT * FROM Users";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<User>(sql);
                return Ok(result.Distinct().ToList());
            }
        }

        /// <summary>
        /// List User teams
        /// </summary>
        /// <remarks>Gets a list of all &#x60;team&#x60; entities for user.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <response code="200">Successful response - returns an array of &#x60;team&#x60; entities.</response>
        [HttpGet("{userId}/teams", Name = "GetUserTeams")]
        public async Task<IActionResult> GetUserTeams([FromRoute] [Required] string userId)
        {
            string sql = 
                @"SELECT teamId, name, userId, firstName, lastName, email, profileIcon FROM Teams JOIN Users ON Teams.teamAdmin = Users.userId
                    WHERE teamId IN (SELECT teamId FROM TeamMembers WHERE userId = @userId) ORDER BY teamId;
                  SELECT * FROM TeamMembers JOIN Users ON TeamMembers.userId = Users.userId
                    WHERE teamId IN (SELECT teamId FROM TeamMembers WHERE userId = @userId) ORDER BY teamId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new {userId});
                var teams = result.Read<Team, User, Team>((team, user) =>
                {
                    team.TeamAdmin = user;
                    team.Members = new List<User>();
                    return team;
                }, "userId").Distinct().ToArray();
                var count = 0;
                var members = result.Read().Distinct().ToList();
                foreach (var m in members)
                {
                    while (count < teams.Length && teams[count].TeamId < m.teamId)
                        count++;
                    if (count < teams.Length && teams[count].TeamId == m.teamId)
                        teams[count].Members.Add(Models.User.Hydrate(m));
                }
                return Ok(teams);
            }
        }
        
        /// <summary>
        /// Get list of checkins a user has been a part of.
        /// </summary>
        /// <param name="userId"></param>
        /// <response code="200">Returns list of checkins. </response>
        [HttpGet("{userId}/checkins")]
        [ValidateModelState]
        [SwaggerOperation("GetUserCheckins")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Checkin>), description: "Returns list of checkins. ")]
        public async Task<IActionResult> GetUserCheckins([FromRoute][Required]string userId)
        { 
            string sql = "SELECT * FROM Checkins WHERE userId = @userId;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {userId});
                return Ok(result.Distinct().ToList());
            }
        }
        
        /// <summary>
        /// Get a user&#39;s current RSVPs
        /// </summary>
        /// <param name="userId">The id of the user</param>
        /// <response code="200">The RSVPs the user currently has</response>
        [HttpGet("{userId}/rsvps", Name = "GetUserRsvps")]
        [ValidateModelState]
        [SwaggerOperation("GetUserRsvps")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Rsvp>), description: "The RSVPs the user currently has")]
        public async Task<IActionResult> GetUserRsvps([FromRoute][Required]string userId, [FromQuery][Optional]DateTime? filterDate)
        {
            filterDate = filterDate ?? System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            string sql =
                @"SELECT * FROM Rsvps INNER JOIN Sessions ON Rsvps.sessionId = Sessions.sessionId
                    WHERE Rsvps.userId = @userId AND Sessions.end > @date;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<Session>(sql, new {userId, date = filterDate});
                return Ok(result.Distinct().ToList());
            }
        }

        /// <summary>
        ///  Gets events that the user has RSVPed for but not checked into.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet("{userId}/sessions", Name = "GetUserRsvpSessions")]
        [ValidateModelState]
        public async Task<IActionResult> GetUserUpcomingRsvpedSessions([FromRoute][Required] string userId, [FromQuery][Optional] DateTime? filterDate)
        {
            filterDate = filterDate ?? System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            string sql =
                @"SELECT EXISTS(SELECT * FROM Users WHERE userId = @userId);
                  SELECT * FROM Rsvps INNER JOIN Sessions ON Rsvps.sessionId = Sessions.sessionId
                    WHERE Rsvps.userId = @userId AND Sessions.end > @date
                    AND NOT Sessions.sessionId IN (
                      SELECT Checkins.sessionId FROM Checkins INNER JOIN Sessions ON Checkins.sessionId = Sessions.sessionId
                      WHERE userId = @userId AND Sessions.end > @date
                    ) ORDER BY start ASC";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryMultipleAsync(sql, new {userId, date = filterDate});
                var userExists = result.ReadFirst<bool>();
                if (!userExists)
                    return BadRequest();
                var rsvps = result.Read<Session>().Distinct().ToList();
                return Ok(rsvps);
            }
        }
        
		/// <summary>
        /// List users matching search
        /// </summary>
        /// <remarks>Gets a list of all &#x60;user&#x60; entities matching a search query.</remarks>
		/// <param name="query">A search term to match for.</param>
        /// <response code="200">Successful response - returns an array of &#x60;user&#x60; entities.</response>
        [HttpGet("search/{query}")]
        public async Task<IActionResult> GetUsersByName([FromRoute][Required] string query)
        {
            query = $"%{query}%";
            string sql = "SELECT * FROM Users WHERE CONCAT(firstName, \" \", lastName) LIKE @query;";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new {query});
                return Ok(result.Distinct().ToList());
            }
        }

        /// <summary>
        /// List All of a user's events
        /// </summary>
        /// <remarks>Gets a list of all &#x60;event&#x60; entities pertaining to a &#x60;user&#x60; entity.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <response code="200">Successful response - returns an array of &#x60;event&#x60; entities.</response>
        [HttpGet("{userId}/events")]
        [ValidateModelState]
        [SwaggerOperation("GetUserEvents")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Event>), description: "Returns list of events of user. ")]
        public async Task<IActionResult> GetUserEvents([FromRoute][Required]string userId)
        {
            string sql = 
                @"select * from Events where eventId in (
                    select eventId from Sessions where sessionId in (
                      select sessionId from Checkins where userId = @userId)
                    ) order by start desc";

            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql, new { userId });
                return Ok(result.Distinct().ToList());
            }
        }
        
        /// <summary>
        /// Update a user
        /// </summary>
        /// <remarks>Updates an existing &#x60;user&#x60;.</remarks>
        /// <param name="userId">A unique identifier for a &#x60;user&#x60;.</param>
        /// <param name="user">Updated &#x60;user&#x60; information.</param>
        /// <response code="202">Successful response.</response>
        [HttpPut("{userId}")]
        [ValidateModelState]
        [SwaggerOperation("UpdateUser")]
        public async Task<IActionResult> UpdateUser([FromRoute][Required]string userId, [FromBody]User user)
        {
            user.UserId = userId;
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_UPDATE.ToFriendlyString(), "user", user);
            if (!await auth.IsSuperAdmin(UserId) && UserId != userId)
                return Unauthorized();
            string sql =
                @"UPDATE Users SET firstName = @firstName, lastName = @lastName, email = @email, profileIcon = @profileIcon
                    WHERE userId = @userId;
                  SELECT * FROM Users WHERE userId = @userId";
            using (IDbConnection conn = new MySqlConnection(db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync(sql,
                    new { userId, firstName = user.FirstName, lastName = user.LastName, email = user.Email, profileIcon = user.ProfileIcon });
                if (result.Any())
                {
                    var newUser = result.FirstOrDefault();
                    log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.UPDATED.ToFriendlyString(), "user", newUser);
                    return Ok(newUser);
                }
                else
                    return NotFound();
            }
        }
    }
}
