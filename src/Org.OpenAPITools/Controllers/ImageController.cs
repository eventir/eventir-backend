using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Org.OpenAPITools.Controllers
{
    [Route("api/image")]
    [Authorize]
    public class ImageController : ControllerBase
    {
        private string UserId => Util.GetUserId(User);
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// Post an Image
        /// </summary>
        /// <remarks>Adds a new instance of a &#x60;IFormFile&#x60; to the system.</remarks>
        /// <param name="image">A new &#x60;IFormFile&#x60; to be posted.</param>
        [HttpPost]
        public async Task<IActionResult> PostImage([FromForm][Required] IFormFile image)
        {
            log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.ATTEMPT_CREATE.ToFriendlyString(), "image", image);
            if (image.Length > 0 && image.ContentType.Contains("image/"))
            {
                SHA1 sha = new SHA1CryptoServiceProvider();
                byte[] imageHash;
                using (var ms = new MemoryStream())
                {
                    await image.CopyToAsync(ms);
                    var fileBytes = ms.ToArray();
                    imageHash = sha.ComputeHash(fileBytes);
                }
                
                var imageType = image.ContentType.Split('/')[1];
                
                var url = $"./wwwroot/images/{Convert.ToBase64String(imageHash).Replace('+', '-').Replace('/', '_')}.{imageType}";
                using (var stream = new FileStream(url, FileMode.Create))
                    await image.CopyToAsync(stream);
                    
                var imageUrl = $"http://{HttpContext.Request.Host}/images/{Convert.ToBase64String(imageHash).Replace('+', '-').Replace('/', '_')}.{imageType}";
                log.Info("User {userId} {action} {type} {@object}", UserId, LogActions.CREATED.ToFriendlyString(), "image", new {imageUrl});
                return Ok(imageUrl);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}