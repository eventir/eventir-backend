using System.Security.Claims;

namespace Org.OpenAPITools
{
    public class Util
    {
        public static string GetUserId(ClaimsPrincipal user)
        {
            return user.FindFirst("sub").Value;
        }
    }
}