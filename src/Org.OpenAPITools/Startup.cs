/*
 * Eventir
 *
 * An API for event management and attendance tracking
 *
 * OpenAPI spec version: 1.1.0
 *
 * Generated by: https://openapi-generator.tech
 */

using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Org.OpenAPITools.Filters;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Org.OpenAPITools
{
    /// <summary>
    ///     Startup
    /// </summary>
    public class Startup
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        ///     The application configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // TODO: use more secure policy, this should only be for testing
            services.AddCors(o => o.AddPolicy("AllowAny", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .AllowAnyHeader();
            }));
            // Add framework services.
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(opts =>
                {
                    opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opts.SerializerSettings.Converters.Add(new StringEnumConverter
                    {
                        CamelCaseText = true
                    });
                });
            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("1.1.0", new Info
                    {
                        Version = "1.1.0",
                        Title = "Eventir",
                        Description = "Eventir (ASP.NET Core 2.0)",
                        Contact = new Contact
                        {
                            Name = "OpenAPI-Generator Contributors",
                            Url = "https://github.com/openapitools/openapi-generator",
                            Email = ""
                        },
                        TermsOfService = ""
                    });
                    c.CustomSchemaIds(type => type.FriendlyId(true));
                    c.DescribeAllEnumsAsStrings();
                    c.IncludeXmlComments(
                        $"{AppContext.BaseDirectory}{Path.DirectorySeparatorChar}{Assembly.GetEntryAssembly().GetName().Name}.xml");

                    // Include DataAnnotation attributes on Controller Action parameters as Swagger validation rules (e.g required, pattern, ..)
                    // Use [ValidateModelState] on Actions to actually validate it in C# as well!
                    c.OperationFilter<GeneratePathParamsValidationFilter>();
                });
            services.Configure<DatabaseSettings>(options =>
                Configuration.GetSection("DatabaseConnectionStrings").Bind(options));
            services.Configure<OpenIdSettings>(options => Configuration.GetSection("OpenIdStrings").Bind(options));
            services.AddSingleton<IAuthorization, Authorization>();
            services.Configure<EmailSettings>(options => Configuration.GetSection("EmailSettings").Bind(options));
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, o =>
                {
                    o.LoginPath = "/api/login";
                    o.LogoutPath = "/api/logout";
                })
                .AddOpenIdConnect(
                    "Google",
                    o =>
                    {
                        o.ClientId = Configuration["OpenIdStrings:GoogleClientId"];
                        o.ClientSecret = Environment.GetEnvironmentVariable("EVENTIR_OPENID_CLIENT_SECRET");
                        o.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                        o.Authority = "https://accounts.google.com";
                        o.ResponseType = OpenIdConnectResponseType.Code;
                        o.SaveTokens = true;
                        o.GetClaimsFromUserInfoEndpoint = true;
                        o.CallbackPath = "/signin-oidc";
                        o.Scope.Add("openid");
                        o.Scope.Add("email");
                        o.Scope.Add("profile");
                    }
                );
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            JwtSecurityTokenHandler.DefaultInboundClaimFilter.Clear();
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAny");
            app
                .UseAuthentication()
                //.UseHttpsRedirection()
                .UseMvc()
                .UseDefaultFiles()
                .UseStaticFiles()
                .UseSwagger(c => { c.RouteTemplate = "swagger/{documentName}/openapi.json"; })
                .UseSwaggerUI(c =>
                {
                    //TODO: Either use the SwaggerGen generated Swagger contract (generated from C# classes)
                    c.SwaggerEndpoint("/swagger/1.1.0/openapi.json", "Eventir");

                    //TODO: Or alternatively use the original Swagger contract that's included in the static files
                    // c.SwaggerEndpoint("/openapi-original.json", "Eventir Original");
                });

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();
        }
    }
}