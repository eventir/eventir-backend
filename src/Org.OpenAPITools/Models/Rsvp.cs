/*
 * Eventir
 *
 * An API for event management and attendance tracking
 *
 * OpenAPI spec version: 1.1.0
 * 
 * Generated by: https://openapi-generator.tech
 */

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Org.OpenAPITools.Models
{ 
    /// <summary>
    /// A single rsvp for a user to a session
    /// </summary>
    [DataContract]
    public partial class Rsvp : IEquatable<Rsvp>
    { 
        /// <summary>
        /// The id of the RSVP
        /// </summary>
        /// <value>The id of the RSVP</value>
        [Required]
        [DataMember(Name="rsvpId")]
        public int? RsvpId { get; set; }

        /// <summary>
        /// Gets or Sets User
        /// </summary>
        [Required]
        [DataMember(Name="user")]
        public User User { get; set; }

        /// <summary>
        /// Gets or Sets Session
        /// </summary>
        // TODO: Update API Curio with change
        [Required]
        [DataMember(Name="sessionId")]
        public int SessionId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Rsvp {\n");
            sb.Append("  RsvpId: ").Append(RsvpId).Append("\n");
            sb.Append("  UserId: ").Append(User).Append("\n");
            sb.Append("  SessionId: ").Append(SessionId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Rsvp)obj);
        }

        /// <summary>
        /// Returns true if Rsvp instances are equal
        /// </summary>
        /// <param name="other">Instance of Rsvp to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Rsvp other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    RsvpId == other.RsvpId ||
                    RsvpId != null &&
                    RsvpId.Equals(other.RsvpId)
                ) && 
                (
                    User == other.User ||
                    User != null &&
                    User.Equals(other.User)
                ) && 
                (
                    SessionId == other.SessionId ||
                    SessionId != null &&
                    SessionId.Equals(other.SessionId)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (RsvpId != null)
                    hashCode = hashCode * 59 + RsvpId.GetHashCode();
                    if (User != null)
                    hashCode = hashCode * 59 + User.GetHashCode();
                    if (SessionId != null)
                    hashCode = hashCode * 59 + SessionId.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

		/// <summary>
        /// Determines equality
        /// </summary>
        /// <returns>Boolean</returns>
        public static bool operator ==(Rsvp left, Rsvp right)
        {
            return Equals(left, right);
        }

		/// <summary>
        /// Determines inequality
        /// </summary>
        /// <returns>Boolean</returns>
        public static bool operator !=(Rsvp left, Rsvp right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
