using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Org.OpenAPITools.Models
{
    /// <summary>
    ///     An email notification
    /// </summary>
    [DataContract]
    public class Notification
    {
        /// <summary>
        ///     The subject line of the email to send
        /// </summary>
        /// <value>The subject</value>
        [Required]
        [DataMember(Name = "title")]
        public string title { get; set; }

        /// <summary>
        ///     The body of the email to send
        /// </summary>
        /// <value>The body</value>
        [Required]
        [DataMember(Name = "body")]
        public string body { get; set; }
        
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Event {\n");
            sb.Append("  Title: ").Append(title).Append("\n");
            sb.Append("  Body: ").Append(body).Append("\n");
            return sb.ToString();
        }
    }
}