using System.Data;
using System.Threading.Tasks;
using Dapper;
using MySql.Data.MySqlClient;

namespace Org.OpenAPITools
{
    // TODO: Integrate this with the built-in authorization interfaces someday.
    // TODO: Fix issues with mismatched types
    public interface IAuthorization
    {
        Task<bool> IsSuperAdmin(string userId);

        Task<bool> IsEventAdmin(string userId, int eventId);

        Task<bool> IsEventAdminForSession(string userId, int sessionId);

        Task<bool> IsSessionAdmin(string userId, int sessionId);

        Task<bool> IsTeamAdmin(string userId, int teamId);
    }
}