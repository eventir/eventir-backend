using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace Org.OpenAPITools
{
    public class Authorization : IAuthorization
    {
        private DatabaseSettings _db;

        public Authorization(IOptions<DatabaseSettings> settings)
        {
            _db = settings.Value;
        }
        
        public async Task<bool> IsSuperAdmin(string userId)
        {
            string sql = "SELECT EXISTS (SELECT * FROM Admins WHERE userId = @userId);";
            using (IDbConnection conn = new MySqlConnection(_db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<bool>(sql, new {userId});
                return result.FirstOrDefault();
            }
        }

        public async Task<bool> IsEventAdmin(string userId, int eventId)
        {
            string sql =
                @"SELECT EXISTS(SELECT * FROM Admins WHERE userId = @userId)
                    OR EXISTS(SELECT * FROM EventAdmins WHERE userId = @userId AND eventId = @eventId);";
            using (IDbConnection conn = new MySqlConnection(_db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<bool>(sql, new {userId, eventId});
                return result.FirstOrDefault();
            }
        }


        public async Task<bool> IsEventAdminForSession(string userId, int sessionId)
        {
            string sql =
                @"SELECT EXISTS(SELECT * FROM Admins WHERE userId = @userId)
                    OR EXISTS(SELECT * FROM EventAdmins WHERE userId = @userId
                    AND eventId = (SELECT eventId FROM Sessions WHERE sessionId = @sessionId));";
            using (IDbConnection conn = new MySqlConnection(_db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<bool>(sql, new {userId, sessionId});
                return result.FirstOrDefault();
            }
        }
        
        public async Task<bool> IsSessionAdmin(string userId, int sessionId)
        {
            string sql =
                @"SELECT EXISTS(SELECT * FROM Admins WHERE userId = @userId)
                    OR EXISTS(SELECT * FROM EventAdmins WHERE userId = @userId AND eventId = (SELECT eventId FROM Sessions WHERE sessionId = @sessionId))
                    OR EXISTS(SELECT * FROM SessionAdmins WHERE userId = @userId AND sessionId = @sessionId);";
            using (IDbConnection conn = new MySqlConnection(_db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<bool>(sql, new {userId, sessionId});
                return result.FirstOrDefault();
            }
        }
       

        public async Task<bool> IsTeamAdmin(string userId, int teamId)
        {
            string sql =
                @"SELECT EXISTS(SELECT * FROM Admins WHERE userId = @userId)
                    OR EXISTS(SELECT * FROM Teams WHERE teamId = @teamId AND teamAdmin = @userId);";
            using (IDbConnection conn = new MySqlConnection(_db.MySQLString))
            {
                conn.Open();
                var result = await conn.QueryAsync<bool>(_db.MySQLString);
                return result.FirstOrDefault();
            }
        }
    }
}