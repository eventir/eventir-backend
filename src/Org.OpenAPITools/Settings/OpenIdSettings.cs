using System;

namespace Org.OpenAPITools
{
    public class OpenIdSettings
    {
        public string GoogleAuthorizationEndpoint { get; set; }
        
        public string GoogleClientId { get; set; }

        public string GoogleClientSecret => Environment.GetEnvironmentVariable("EVENTIR_OPENID_CLIENT_SECRET");
        
        public string GoogleRedirectUri { get; set; }
        
        public string LoginUrl { get; set; }
        
        public string HomeUrl { get; set; }
    }
}