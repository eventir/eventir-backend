using System;

namespace Org.OpenAPITools
{
    public class DatabaseSettings
    {
        private string mySQLString;
        
        public string MySQLString
        {
            get { return mySQLString; }
            set { mySQLString = value + Environment.GetEnvironmentVariable("EVENTIR_DATABASE_USER_PASSWORD"); }
        }
    }
}